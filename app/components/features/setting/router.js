import React from "react";
import { Router, Stack, Scene, Actions } from "react-native-router-flux";

//Pages
import { Setting } from './setting'

const settingRouter = () => {
  return (
    <Stack key='settingStack' title='Setting'>
      <Scene
        initial
        key='keySetting'
        title='Setting'
        component={Setting}
      />
    </Stack>
  )
}

export default settingRouter;