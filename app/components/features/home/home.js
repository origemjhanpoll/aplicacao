import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux'

export function Home(props) {
  const [nome, setNome] = useState({ nome: 'Jean Paul' })

  useEffect(() => {
    console.log('entrou no home', props)
  }, []);

  return (
    <View>
      <Text>Entrou na Home: {nome.nome}</Text>
      <TouchableOpacity
        onPress={() => Actions.jump('keySetting', {
          screenProps: {
            nome: 'Jean Paul',
            idade: 19,
            telefone: 23233232
          }
        })}
        style={{ padding: 16, backgroundColor: 'blue', justifyContent: 'center' }}>
        <Text>Botão de ação</Text>
      </TouchableOpacity>
    </View>
  );
}