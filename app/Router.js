import React from "react";
import { Router, Stack, Scene, Actions } from "react-native-router-flux";

import HomeRouter from './components/features/home/router'
import Setting from './components/features/setting/router'

const RouterComponent = () => {
  return (
    <Router>
      <Stack tabs showLabel={true}
        tabBarStyle={{ paddingBottom: 14 }}
        labelStyle={{ fontSize: 14 }}
      >
        {HomeRouter()}
        {Setting()}
      </Stack>

    </Router>
  )
}

export default RouterComponent;